# Linux Box Software Setup

####   This README pertains to the Linux setup project, a collection of Ansible files whose aim is to get a clean Linux environment ready for development by ensuring the presence of my preferred tools.

---

** The tools in question: **

* Virtualenvwrapper
* jq
* yq
* curl
* Terminator
* Octave
* Atom


#### How to execute:
``` ansible-playbook --ask-become-pass setup.yml ```
* The playbook itself is ```setup.yml```, which contains the instructions for what to install.
* The ```--ask-become-pass```  will prompt the user for their sudo password before execution. Tasks with ```become: true``` as an argument escalate to the super user to run.






# Add packages for atom
# Add pycharm
# Add dbeaver

